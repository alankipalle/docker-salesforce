#Docker SalesForce#
A dockerfile for a stripped down container for deploying code to SalesForce.com via the [SalesForce Migration Tool](https://developer.salesforce.com/docs/atlas.en-us.daas.meta/daas/)
##Base Image contains##
* Alpine
* bash
* Java
* ant
Salesforce Migration Tool (ant lib)
##How to Use#
Refer to the Docker Container documentation on how to manage containers.  Once you have the container up and running you can test and deploy your SalesForce code with ant and your buildfile.

##Example Bitbucket Pipeline Integration##
An example Bitbucket Pipeline is also included.  The example shows how to use BitBucket Pipelines to test salesforce code.  To use the example you need to add the Required environment variables to your Pipeline environment variables.  The example will execute the deployCodeCheckOnly target to simulate running the tests for the salesforce.com code.

##Required Variables For SalesForce.com##
* SF_USERNAME
* SF_PASSWORD
* SF_SERVERURL